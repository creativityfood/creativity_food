<x-layout
    title="Modifica ricetta"
    description="Form per modificare la ricetta"
>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 text-center m-4 ">
                <h1 class="fa-3x fw-bolder">{{__('Modifica la tua ricetta')}}</h1> 
            </div>
            @if ($errors->any()) 
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                <form method="POST" action="{{route('recipe.update', compact('recipe'))}}"enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                      <label for="exampleInputtext1" class="form-label fw-bolder">Modifica il titolo</label>
                      <input type="text" class="form-control border-dark" name="title" value="{{$recipe->title}}">
                    </div>
                     <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label fw-bolder">Modifica Ingredienti</label>
                        <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="ingredients" rows="3">{{old('ingredients')}}{{$recipe->ingredients}}</textarea>
                      </div>
                      <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label fw-bolder">Modifica preparazione</label>
                        <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="preparation" rows="3">{{old('preparation')}}{{$recipe->preparation}}</textarea>
                      </div>
                      <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label fw-bolder">Modifica descrizione</label>
                        <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="description" rows="3">{{old('description')}}{{$recipe->description}}</textarea>
                      </div>
                   
                    <div class="mb-4 text-center ">
                        <button type="submit" class="btn btn-modifica">{{ __('Invia')}}</button>
                    </div>
                    <div class="mb-4 text-center ">
                        <a href="{{route('recipe.show', compact('recipe'))}}" class="btn btn-annulla">{{ __('Annulla')}}</a>
                    </div>
                  </form>
            </div>
        </div>
    </div>
</x-layout>