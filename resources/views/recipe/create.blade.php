<x-layout
    title="Crea ricetta"
    description="Form per inserire la ricetta"
>
    <div class="container">
        <div class="row justify-content-center">
    
            <div class="col-12 text-center p-4">
                <h1 class="fw bolder fa-3x">{{ __('Inserisci la ricetta')}}</h1>
            </div>
                @if ($errors->any()) 
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            <div class="col-12 col-md-6 p-3">
                <form method="POST" action="{{route('recipe.store')}}"enctype="multipart/form-data">
                @csrf
                    <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">
                    <div class="mb-3">
                        <label for="exampleInputtext1" class="form-label fw-bolder">{{ __('Inserisci il titolo')}}</label>
                        <input type="text" class="form-control border-dark" name="title" value="{{old('title')}}">
                    </div>
                    <label class="form-label fw-bolder mt-2">{{ __('Seleziona la portata')}}</label>
                        <select name="course_id" class=" text-center m-3 p-2"> 
                            @foreach($courses as $course)
                                <option  value="{{$course->id}}">{{$course->name}}</option>
                            @endforeach
                        </select>

                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label fw-bolder">{{ __('Inserisci descrizione')}}</label>
                        <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="description" rows="3">{{old('description')}}</textarea>
                    </div>
                    
                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label fw-bolder">{{ __('Inserisci ingredienti')}}</label>
                        <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="ingredients" rows="3">{{old('ingredients')}}</textarea>
                    </div>

                    <div class="mb-3">
                        <label for="exampleFormControlTextarea1" class="form-label fw-bolder">{{ __('Inserisci preparazione')}}</label>
                        <textarea class="form-control border-dark" id="exampleFormControlTextarea1" name="preparation" rows="3">{{old('preparation')}}</textarea>
                    </div>

                    <div class="form-group row">
                        <label for="images" class="col-md-12 col-form-label text-md-left fw-bolder">{{ __('Inserisci immagini')}}</label>
                        <div class="col-12">
                            <div class="dropzone border border-dark" id="drophere"></div>
                        </div>
                    </div>

                    <div class="my-4 text-center ">
                        <button type="submit" class="btn btn-modifica">{{ __('Invia')}}</button>
                    </div>

                    <div class="mb-4 text-center ">
                        <a href="{{route('homepage')}}" class=" btn btn-elimina">{{ __('Annulla')}}</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-layout>