<x-layout
  title="{{$recipe->title}}"
  description="{{$recipe->description}}"
>

    <div class="container">
        <div class="row justify-content-center align-items-center" itemscope itemtype="https://schema.org/Recipe">

{{-- Title --}}
            <div class="col-12 text-center">
              <span itemprop="recipeName">
                <h1 class="fw-bolder fa-3x p-3">{{$recipe->title}}</h1>
              </span>
              <span itemprop="recipeCategory">
                 <a class="btn my-3 text-decoration-underline" href="{{route('recipe.course', ['course'=>$recipe->course->id])}}">
                  {{$recipe->course->name}}</a>
              </span>
               
            </div>
            
{{-- Carousel with Images --}}
            <div class="col-12 col-md-6 mb-3 ">
              <ol class="carousel-indicators">
                @foreach ($recipe->images as  $image)
                   <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                @endforeach
               </ol>
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-inner text-center">
                    @foreach ($recipe->images as $image)
                      <div class="carousel-item d-block {{ $loop->first ? 'active' : '' }}">
                        <img itemprop="image" alt="dettaglio del piatto {{$recipe->title}}" class="border border-danger p-2 img-fluid" src="{{$image->getUrl(500, 480)}}">
                      </div>
                    @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
                      <span aria-hidden="true"><i class="text-dark fa-solid fa-circle-chevron-left"></i></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
                      <span aria-hidden="true"> <i class=" text-dark fa-solid fa-circle-chevron-right"></i></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                  </div>
            </div>

{{-- Article body --}}
            <div class="col-12 col-md-6 p-2 d-flex flex-column  justify-content-center">
              <div class="col-12">
                <div class="p-5 container-body">
                  <h3 class="text-center fw-bolder">{{ __('Ricetta')}}</h3>
                  <h5 class="my-3 text-decoration-underline"> {{ __('Ingredienti')}}</h5>
                  <div  class="my-5">
                    @foreach($ingredients as $ingredient)
                    <ul>
                      <span itemprop="recipeIngredient">
                          <li>{{$ingredient}}</li>
                      </span>
                    </ul>
                    @endforeach
                  </div>
                      <h5 class="mb-4 mt-4 text-decoration-underline">{{ __('Preparazione')}}</h5>
                      <span itemprop="recipeInstructions">
                        <p class="mt-2">{!! nl2br(e($recipe->preparation)) !!}</p>
                      </span>
                </div>
              </div>
              <div class="col-12 text-end">
                <meta class="card-text p-2" itemprop="datePublished" content="05-11-2021">
                <small>{{__('Pubblicata: ')}}<em>{{$recipe->created_at->format('d.m.Y')}}</em></small>
              </div>
            </div>
        </div>

{{-- Button--}}
        @auth 
        <div class="col-12 text-center my-2">
          @if(Auth::user()->is_admin)
            <a href="{{route('recipe.edit', compact('recipe'))}}" class="btn btn-modifica">{{ __('Modifica')}}</a>
        </div>
        <div class="col-12 text-center my-2">
            <form action="{{route('recipe.delete', compact ('recipe'))}}" method="POST">
              @csrf
              @method('delete')
              <button type="submit" class="btn btn-elimina">{{ __('Elimina')}}</button>
            </form>
          @endif
        </div>
        @endauth
        <div class="text-center mt-2">
          <a class="btn btn-click" href="javascript:history.go(-1)" 
              onMouseOver="self.status=document.referrer;return true">
              <i class="fa-solid fa-circle-left"></i>
          </a>
          <a href="{{route('homepage')}}" class="btn btn-modifica my-5"><i class="fa-solid fa-house-chimney"></i></a>
        </div>

{{--Share Section--}}
        <div class="row justify-content-center align-items-center">
            <div class="col-12 d-flex justify-content-center align-items-center">
                <h3>{{ __('Condividi la ricetta')}}</h3>
            </div>
            <div class="col-6 d-flex justify-content-center align-items-center">
                <a class="m-2" href="https://www.pinterest.ch/pin/create/button/?media=https://www.lachiavenelpozzo.com/img/pozzo.jpg&url=https://dettaglio-ricetta/{article}">
                    <i class="text-danger fa-brands fa-pinterest"></i>
                </a>
                <a class="m-2" href="https://www.facebook.com/sharer/sharer.php?u=https://dettaglio-ricetta/{article}">
                    <i class="fa-brands fa-facebook"></i>
                </a>
                <a class="m-2" href="https://www.instagram.com/p/CQY28sHFaXB/?utm_source=ig_embed&amp;utm_campaign=loading">
                    <i class="text-dark instagram fa-brands fa-instagram"></i>
                </a>
            </div>
        </div>
    </div>
</x-layout>