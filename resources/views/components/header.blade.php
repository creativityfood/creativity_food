<header>
    <div class="container-fluid header ">
        <div class="container d-flex align-items-space-around justify-content-center mt-5">
            <div class="row">
                <div class="col-12 text-center d-flex align-items-center justify-content-center">
                    <div class="title fw-bolder fa-4x">Creativity Food</div>
                </div><br>
                <h4 class="text-center text-white fw-bolder">Ricette facili e golose</h4> 
                <div class="col-12 d-flex justify-content-center align-items-end mb-5">
                    <form action="{{route('search')}}" method="GET" class="text-center">
                        <div class="input-group container-searchbar">
                            <input type="text" class="p-2 searchbar" placeholder=" {{ __('Cerca la ricetta')}}" name="q" aria-label="Cerca">
                            <button class="btn" type="submit" id="button-addon2"><i class="fa-solid fa-magnifying-glass text-white iconsearch"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</header>