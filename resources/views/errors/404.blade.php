<x-layout
    title="404 Pagina non trovata"
    description="404 Pagina non trovata"
>
    <div class="container">
        <div class="row justify-content-center align-items-center my-5">

{{-- Title --}}
            <div class="col-12 text-center">
                <h1>{{__('Pagina non trovata')}}</h1>
            </div>


        </div>
        
{{-- Button--}}
        <div class="mb-4 text-center">
           
            <a href="{{route('homepage')}}" class="btn btn-click my-5"> <h3>{{__('Torna alla Home')}}</h3></a>
        </div>

    </div>

</x-layout>