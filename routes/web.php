<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RecipeController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Rotte pubbliche

Route::get('/', [PublicController::class, 'index'])->name('homepage');

Route::get('/dettaglio-ricetta/{recipe}', [PublicController::class, 'show'])->name('recipe.show');

Route::get('/cerca', [PublicController::class, 'search'])->name('search');

Route::get('/portata/{course}/ricetta', [PublicController::class, 'recipeByCourse'])->name('recipe.course');

Route::get('/contact', [PublicController::class, 'contact'])->name('contact');


// Rotte creatori contenuti

Route::get('/inserisci-ricetta', [RecipeController::class, 'create'])->name('recipe.create');

Route::post('/crea-ricetta', [RecipeController::class, 'store'])->name('recipe.store');

Route::get('/modifica-ricetta/{recipe}', [RecipeController::class, 'edit'])->name('recipe.edit');

Route::put('/edita-ricetta/{recipe}',[RecipeController::class, 'update'])->name('recipe.update');

Route::delete('/elimina-ricetta/{recipe}',[RecipeController::class, 'destroy'])->name('recipe.delete');


// Rotte immagini

Route::post('/recipe/images/upload', [RecipeController::class, 'uploadImage'])->name('recipe.image.upload');

Route::delete('/recipe/images/remove', [RecipeController::class, 'removeImage'])->name('recipe.image.remove');

Route::get('/recipe/images/', [RecipeController::class, 'getImages'])->name('recipe.images');