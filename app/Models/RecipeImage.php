<?php

namespace App\Models;

use App\Models\Recipe;
use App\Models\RecipeImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RecipeImage extends Model
{
    protected $casts =[
        'labels' => 'array',
    ];
    
    public function recipe(){
        
        return $this->belongsTo(Recipe::class);
    }

    static public function getUrlByFilePath($filePath, $w = null, $h = null)
    {
        if(!$w && !$h){
            return Storage::url($filePath);
        }

        $path = dirname($filePath);
        $filename = basename($filePath);

        $file = "{$path}/crop{$w}x{$h}_{$filename}";

        return Storage::url($file);
    }

    public function getUrl($w = null, $h = null)
    {
        return RecipeImage::getUrlByFilePath($this->file, $w, $h);
    }
}
