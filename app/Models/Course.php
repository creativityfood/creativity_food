<?php

namespace App\Models;

use App\Models\Course;
use App\Models\Recipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Course extends Model
{
    use HasFactory;
    protected $fillable =[
        'name',
    ];

    public function recipes(){
        return $this->hasMany(Recipe::class);
    }
}