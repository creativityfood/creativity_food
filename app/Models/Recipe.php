<?php

namespace App\Models;

use App\Models\User;
use App\Models\Course;
use App\Models\Recipe;
use App\Models\RecipeImage;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Recipe extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'title',
        'ingredients',
        'preparation',
        'description',
        'user_id',
        'course_id',
    ];
    // Problema con tnt search e laravel forge
    // public function toSearchableArray()
    // {
    //     $course= $this->course;

    //     $array = [
    //         'course'=>$course,
    //         'id' => $this->id,
    //         'title' => $this->title,
    //         'ingredients'=>$this->ingredients,
    //     ];

        

    //     return $array;
    // }
    public function user(){
        return $this->belongsTo(User::class);
        }

        
    public function course(){
        return $this->belongsTo(Course::class);
    }

    public function images(){
        return $this->hasMany(RecipeImage::class);
    }

    public function getRouteKeyName()
    {
        return 'title';
    }
}