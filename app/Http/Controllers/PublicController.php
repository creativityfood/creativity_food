<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Recipe;
use Illuminate\Http\Request;

class PublicController extends Controller
{
     // Rotta homepage
     public function index()

     {  $courses = Course::all();
        $recipes = Recipe::orderBy('created_at', 'DESC')
        ->with(['user', 'course'])
        ->take(6)
        ->get();

        return view('welcome', compact ('recipes', 'courses'));
     }
 
     //    mostra il dettaglio della risorsa
    public function show(Recipe $recipe)
    {   $courses = Course::all();
        $ingredients = explode(',', $recipe->ingredients);

        return view ('recipe.show', compact('recipe', 'ingredients', 'courses'));
    }

    // funzione per la ricerca full text
    public function search(Request $request){

        // prendiamo quello che scriviamo nella barra
        $q = $request->input('q');

        // lo cerchiamo e lo assegniamo all'oggetto
        $recipes = Recipe::whereRelation('course', 'name', 'like', '%'.$q.'%')
            ->orWhere('title', 'like', '%'.$q.'%')
            ->orWhere('ingredients', 'like', '%'.$q.'%')
            ->orderBy('title', 'ASC')
            ->get();

        return view ('search_result', compact('q', 'recipes'));
    }

    // funzione per la ricerca per portata
    public function recipeByCourse ($id){

        // cerchiamo l'id della la portata
        $course = Course::find($id);
        // attraverso il vincolo di relazione
        $recipes = $course -> recipes()
        // se lo trova lo mostra
        ->orderBy('title', 'ASC')
        ->paginate(6);

        return view('search_result_by_course', compact('course', 'recipes'));
    }
    
    // funzione per la pagina contatti
    public function contact(){
        return view('contact');
    }
}
