<?php

namespace App\Providers;

use App\Models\Course;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {       Paginator::useBootstrap();

    
            if(Schema::hasTable('courses')){
    
                $courses = Course::all();
    
                View::share('courses', $courses);
            }
            
            
    }
}
